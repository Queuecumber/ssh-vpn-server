# SSH VPN Server

OpenSSH provides rudimentary VPN capabilities using tun/tap devices.
This project aims to make it easier to work with this VPN by automatically
configuring users, their tun/tap devices, and their login keys and bridging
their devices to the network. This software is intended to be run in a docker 
container, probably with host networking and definitly with CAP_NET_ADMIN.
The SSH server which is provided by default cannot be logged into, and cannot 
forward local ports. It only provides the tunneling required for VPN.  

By default the docker container does not contain any user configuration. You
should create your own configuration and mount it at `/etc/ssh-vpn/ssh-vpn-config.yaml`
or mount it wherever you want and pass the location as an argument. A custom `sshd_config`
can be used by mounting it at the canonical location e.g. `/etc/ssh/sshd_config`.